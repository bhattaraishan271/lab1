**LAB 01**

The code for linear and binary search algorithms have been implemented in search.py .
Some test cases have been implemented in binaryTest.py for both algorithms.
A screenshot of input-size vs execution-time graph is attached below. The code for generating this graph can be found in searchtime.py .
https://prnt.sc/newbnv

From this graph we can see that as the size increases binary search becomes more effective than linear search. At smaller sizes linear search is just as effective as binary search and can be easier to implement.